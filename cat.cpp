///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}













//////////////////////////////////////////////CAT /\/\/\/\/\ ////////////////////////////

//////////////////////////////////////////////CAT EMPIRE \/\/\/\/\/ /////////////////////


///Things to add addCat, empty, Generation
//PUBLIC

void CatEmpire::addCat( Cat* newCat){
   
   if( topCat == nullptr){  //account for starting a scratch list
      topCat = newCat;
      return;
   }
   

      addCat( topCat, newCat); //outsource to private call
   
}

bool CatEmpire::empty(){ //returns true(1) if list is empty
   return topCat == nullptr;
};

void CatEmpire::catGenerations() const{
         //implement
   if(topCat == nullptr){
      return;}
   bfs();

};


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}





void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}


//CATEMPIRE::PRIVATE
//

void CatEmpire::addCat( Cat* atCat, Cat* newCat){
         //from slides
   if( atCat->name > newCat->name){
      if(atCat->left == nullptr){
            atCat->left = newCat;
      }
      else {
         addCat( atCat->left, newCat);
      }
   }

   if(atCat->name < newCat->name) {
      if( atCat->right == nullptr){
         atCat->right = newCat;
      }else{
         addCat(atCat->right, newCat);
      }
   }
};

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
   
   //if at null
   if(atCat == nullptr){
      return;
   }

   if( atCat->right = nullptr){
      dfsInorderReverse(atCat->right, depth+1); //changes depth as no more exist at this level
   }

   cout<< string(6*(depth-1), ' ') << atCat->name; //prints name with 6 spaces * depth

   if(atCat->left == nullptr && atCat->right == nullptr){
      cout<<endl; //defined in lab manual
   }
   
   if(atCat->left != nullptr && atCat->right == nullptr){
      cout<< "\\" <<endl;
   }

   if(atCat->left != nullptr && atCat->right != nullptr){ //meaning we are not at end
      cout << "<" << endl;
   }
   
   if(atCat->left == nullptr && atCat->right != nullptr){
      cout<<"/" <<endl;
   }

   if(atCat->left != nullptr){
      dfsInorderReverse(atCat->left, depth+1);
   }
   
}

void CatEmpire::dfsInorder( Cat* atCat) const{
   if(atCat == nullptr){
      return;}

   dfsInorder( atCat->left);
   cout<<atCat->name<<endl;
   dfsInorder(atCat->right);

}

void CatEmpire::dfsPreorder( Cat* atCat) const{
   if(atCat == nullptr){
      return;}

   if(atCat->left != nullptr && atCat->right != nullptr){
      cout<<atCat->name<<"begat"<<atCat->left->name<<"and"<<atCat->right->name<<endl;
   }
   if(atCat->left != nullptr && atCat->right == nullptr){ //if no left cat, print center and right
      cout<<atCat->name<<"begat"<<atCat->left->name<<endl;
   }
   if(atCat->left == nullptr && atCat->right != nullptr){ //if no right cat, print left and center
      cout<<atCat->right->name<<"begat"<<atCat->name<<endl;
   }
   
   if(atCat->left == nullptr && atCat->right == nullptr){ //no cats left
      return;
   }

   dfsPreorder(atCat->left);
   dfsPreorder(atCat->right);

}

void CatEmpire::getEnglishSuffix(int n) const{
      // 1"st" 2"nd" 3"rd" 4"th" 

      //special 11th, 12th, 13th (hundred)
      if(n <10){
            if(n == 1){
               cout<<n<<"st"<<endl;
            }
            else if(n == 2){
               cout<<n<<"nd"<<endl;
            }
            else if(n == 3){
               cout<<n<<"rd"<<endl;
            }
            else{
               cout<<n<<"th"<<endl;
            }
      }
      if(n<20 && n>10){
               cout<<n<<"th"<<endl;
      }
      if(n>20){
        int x = n%10;
          if(x==1){
               cout<<n<<"st"<<endl;
            }
            else if(x==2){
               cout<<n<<"nd"<<endl;
            }
            else if(x==3){
               cout<<n<<"rd"<<endl;
            }
            else{
               cout<<n<<"th"<<endl;
            }
      }

}

void CatEmpire::bfs(){
   queue<Cat*> catQueue;

   catQueue.push(topCat);
   while(catQueue.empty() != 1){
      Cat* cat = catQueue.front();
      catQueue.pop();
   }
   if(catQueue.empty() == 1){
      return;
   }
   if( cat->left != nullptr){
      catQueue.push(cat->left);
   }
   if(cat->right != nullptr){
      catQueue.push(cat->right);
   }

}
